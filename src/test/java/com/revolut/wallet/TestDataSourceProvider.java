package com.revolut.wallet;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Optional;

public final class TestDataSourceProvider {

    private final HikariDataSource dataSource;

    private TestDataSourceProvider() {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(Optional
                .ofNullable(System.getProperty("wallet.jdbc_url", System.getenv("WALLET_JDBC_URL")))
                .orElse("jdbc:h2:mem:wallet;DB_CLOSE_DELAY=-1"));
        config.setUsername(Optional.ofNullable(System.getenv("WALLET_JDBC_USER"))
                .orElse(""));
        config.setPassword(Optional.ofNullable(System.getenv("WALLET_JDBC_PASSWORD"))
                .orElse(""));
        dataSource = new HikariDataSource(config);

        migrate(dataSource);
    }

    private void migrate(final DataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.clean();
        flyway.migrate();
    }

    public static synchronized TestDataSourceProvider getInstance() {
        return new TestDataSourceProvider();
    }

    public HikariDataSource getDataSource() {
        return dataSource;
    }
}
