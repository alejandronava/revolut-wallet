package com.revolut.wallet.domain.vo;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static com.revolut.wallet.util.AmountUtil.round;

@RunWith(DataProviderRunner.class)
public class AccountOperationTest {

    @DataProvider
    public static Object[] validAmounts() {
        return new Object[]{round(new BigDecimal(15.75)), round(new BigDecimal(29.59))};
    }

    @DataProvider
    public static Object[] invalidAmounts() {
        return new Object[]{round(BigDecimal.ZERO), round(new BigDecimal(-15.75)), round(new BigDecimal(-29.59))};
    }

    @Test
    @UseDataProvider("validAmounts")
    public void testDebitOperation(final BigDecimal amount) {
        Assert.assertThat(new AccountOperation.DebitOperation(amount).getAmount(), Is.is(amount.negate()));
    }

    @Test(expected = IllegalArgumentException.class)
    @UseDataProvider("invalidAmounts")
    public void testDebitOperationInvalidArgument(final BigDecimal amount) {
        new AccountOperation.DebitOperation(amount);
    }

    @Test
    @UseDataProvider("validAmounts")
    public void testCreditOperation(final BigDecimal amount) {
        Assert.assertThat(new AccountOperation.CreditOperation(amount).getAmount(), Is.is(amount));
    }

    @Test(expected = IllegalArgumentException.class)
    @UseDataProvider("invalidAmounts")
    public void testCreditOperationInvalidArgument(final BigDecimal amount) {
        new AccountOperation.CreditOperation(amount);
    }
}
