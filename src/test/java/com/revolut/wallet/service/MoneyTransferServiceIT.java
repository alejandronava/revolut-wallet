package com.revolut.wallet.service;

import com.revolut.wallet.TestDataSourceProvider;
import com.revolut.wallet.domain.Account;
import com.revolut.wallet.domain.Transfer;
import com.revolut.wallet.domain.vo.AccountOperation;
import com.revolut.wallet.domain.vo.MoneyTransferInfo;
import com.revolut.wallet.domain.vo.MoneyTransferRequest;
import com.revolut.wallet.repository.*;
import com.revolut.wallet.service.exception.InsufficientBalanceException;
import com.revolut.wallet.util.ConnectionManager;
import com.revolut.wallet.util.ConnectionManagerThreadLocalImpl;
import com.revolut.wallet.util.validator.MoneyTransferRequestValidator;
import com.revolut.wallet.util.validator.ValidatorFactory;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MoneyTransferServiceIT {

    private static final String SENDER_ID = "466049c8-2a34-45b3-941d-d2ce38903fe3";
    private static final String RECEIVER_ID = "cdfa6cf0-1f56-4040-897a-2a0787e58a9a";
    private static final String THIRD_PARTY_ID = "945065b3-78d4-4717-a1d0-e41e08faa883";

    private static AccountRepository accountRepository;
    private static ConnectionManager connectionManager;
    private static TransferRepository transferRepository;
    private static ValidatorFactory validatorFactory;
    private static MoneyTransferRequestValidator moneyTransferRequestValidator;
    private static MoneyTransferService service;

    @BeforeClass
    public static void setupClass() {
        connectionManager = spy(new ConnectionManagerThreadLocalImpl(TestDataSourceProvider.getInstance().getDataSource()));
        accountRepository = spy(new AccountRepositoryH2Impl(connectionManager));
        transferRepository = spy(new TransferRepositoryH2Impl(connectionManager));
        validatorFactory = mock(ValidatorFactory.class);
        moneyTransferRequestValidator = mock(MoneyTransferRequestValidator.class);
        service = new MoneyTransferService(accountRepository, connectionManager, transferRepository, validatorFactory);

        when(validatorFactory.getMoneyTransferRequestValidator()).thenReturn(moneyTransferRequestValidator);
    }

    @Before
    public void cleanUp() {
        reset(accountRepository, connectionManager, transferRepository);

        when(moneyTransferRequestValidator.isValid(any())).thenReturn(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSendInvalidRequest() {
        when(moneyTransferRequestValidator.isValid(any())).thenReturn(false);
        service.send(new MoneyTransferRequest(SENDER_ID, RECEIVER_ID, THIRD_PARTY_ID, new BigDecimal(10.00)));
    }

    @Test
    public void testSendConcurrency() {
        final AtomicInteger succeeded = new AtomicInteger();
        final AtomicInteger failed = new AtomicInteger();
        final int numberOfThreads = 100;
        threadExecution(() -> {
            try {
                AccountRepository accountRepository = spy(new AccountRepositoryH2Impl(connectionManager));
                ResultCaptor<Account> accountCaptor = new ResultCaptor<>();
                doAnswer(accountCaptor).when(accountRepository).getById(anyString());

                TransferRepository transferRepository = spy(new TransferRepositoryH2Impl(connectionManager));
                ResultCaptor<Transfer> transferCaptor = new ResultCaptor<>();
                doAnswer(transferCaptor).when(transferRepository).save(any());

                final MoneyTransferRequest request = new MoneyTransferRequest(SENDER_ID, RECEIVER_ID, THIRD_PARTY_ID,
                        new BigDecimal(15.00))
                        .withRemoteAddress("127.0.0.1");
                final MoneyTransferInfo response = new MoneyTransferService(accountRepository, connectionManager,
                        transferRepository, validatorFactory).send(request);

                verify(accountRepository).getById(SENDER_ID);
                verify(accountRepository).getById(RECEIVER_ID);

                final Account senderAccount = accountCaptor.getAllValues().get(0);
                verify(accountRepository).updateBalance(senderAccount, new AccountOperation.DebitOperation(request.getAmount()));

                final Account receiverAccount = accountCaptor.getAllValues().get(1);
                verify(accountRepository).updateBalance(receiverAccount, new AccountOperation.CreditOperation(request.getAmount()));

                verify(transferRepository).save(new Transfer()
                        .setSenderId(senderAccount.getId())
                        .setSenderPreviousBalance(senderAccount.getBalance())
                        .setSenderCurrentBalance(senderAccount.getBalance().subtract(request.getAmount()))
                        .setReceiverId(receiverAccount.getId())
                        .setReceiverPreviousBalance(receiverAccount.getBalance())
                        .setReceiverCurrentBalance(receiverAccount.getBalance().add(request.getAmount()))
                        .setThirdPartyId(THIRD_PARTY_ID)
                        .setRemoteAddress(request.getRemoteAddress())
                        .setAmount(request.getAmount()));

                assertThat(response, Is.is(new MoneyTransferInfo(transferCaptor.getValue().getId(),
                        senderAccount.getBalance().subtract(request.getAmount()))));

                succeeded.incrementAndGet();
            } catch (final InsufficientBalanceException | ConcurrentModificationException e) {
                failed.incrementAndGet();
            }
        }, numberOfThreads);

        assertThat(succeeded.get(), Is.is(1));
        assertThat(failed.get(), Is.is(numberOfThreads - succeeded.get()));
    }

    private void threadExecution(final Runnable process, final int numberOfThreads) {
        try {
            final Thread[] threads = new Thread[numberOfThreads];
            for (int i = 0; i < numberOfThreads; i++) {
                threads[i] = new Thread(process);
                threads[i].start();
            }
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (final InterruptedException e) {
            fail(e.getLocalizedMessage());
        }
    }

    static class ResultCaptor<T> implements Answer {
        private List<T> values = new ArrayList<>();

        List<T> getAllValues() {
            return values;
        }

        T getValue() {
            return values.isEmpty() ? null : values.get(values.size() - 1);
        }

        @Override
        @SuppressWarnings("unchecked")
        public T answer(final InvocationOnMock invocationOnMock) throws Throwable {
            values.add((T) invocationOnMock.callRealMethod());
            return getValue();
        }
    }

}
