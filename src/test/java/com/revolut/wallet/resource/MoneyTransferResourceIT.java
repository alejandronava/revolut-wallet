package com.revolut.wallet.resource;

import com.revolut.wallet.ApplicationBinder;
import com.revolut.wallet.TestDataSourceProvider;
import com.revolut.wallet.domain.vo.ErrorResponse;
import com.revolut.wallet.domain.vo.MoneyTransferInfo;
import com.revolut.wallet.domain.vo.MoneyTransferRequest;
import com.revolut.wallet.util.AmountUtil;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@RunWith(DataProviderRunner.class)
public class MoneyTransferResourceIT extends JerseyTest {

    private static final String SENDER_ID = "466049c8-2a34-45b3-941d-d2ce38903fe3";
    private static final String RECEIVER_ID = "cdfa6cf0-1f56-4040-897a-2a0787e58a9a";
    private static final String THIRD_PARTY_ID = "945065b3-78d4-4717-a1d0-e41e08faa883";

    @Override
    protected Application configure() {
        final ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(new ApplicationBinder(TestDataSourceProvider.getInstance().getDataSource()));
        resourceConfig.register(MoneyTransferResource.class);
        return resourceConfig;
    }

    @DataProvider
    public static Object[][] requests() {
        return new Object[][]{
                {new BigDecimal(4.50), AmountUtil.round(new BigDecimal(15.50))},
                {new BigDecimal(9.50), AmountUtil.round(new BigDecimal(10.50))},
                {new BigDecimal(7.25), AmountUtil.round(new BigDecimal(12.75))}
        };
    }

    @DataProvider
    public static Object[] insufficientBalance() {
        return new Object[]{new BigDecimal(35.00), new BigDecimal(20.10)};
    }

    @Test
    public void testSendBadRequest() {
        final Response response = target("money-transfer")
                .request()
                .post(Entity.entity(new MoneyTransferRequest(SENDER_ID, SENDER_ID, THIRD_PARTY_ID, BigDecimal.ZERO),
                        MediaType.APPLICATION_JSON_TYPE));
        Assert.assertThat(response.getStatus(), Is.is(Response.Status.BAD_REQUEST.getStatusCode()));

        final ErrorResponse errorResponse = response.readEntity(ErrorResponse.class);
        Assert.assertThat(errorResponse.getMessage(), Is.is("Invalid request"));
    }

    @Test
    @UseDataProvider("insufficientBalance")
    public void testSendInsufficientBalance(final BigDecimal amount) {
        final Response response = target("money-transfer")
                .request()
                .post(Entity.entity(new MoneyTransferRequest(SENDER_ID, RECEIVER_ID, THIRD_PARTY_ID, amount),
                        MediaType.APPLICATION_JSON_TYPE));
        Assert.assertThat(response.getStatus(), Is.is(Response.Status.PRECONDITION_FAILED.getStatusCode()));

        final ErrorResponse errorResponse = response.readEntity(ErrorResponse.class);
        Assert.assertThat(errorResponse.getMessage(), Is.is("Insufficient balance to complete the transfer"));
    }

    @Test
    @UseDataProvider("requests")
    public void testSend(final BigDecimal amount, final BigDecimal balance) {
        final Response response = target("money-transfer")
                .request()
                .post(Entity.entity(new MoneyTransferRequest(SENDER_ID, RECEIVER_ID, THIRD_PARTY_ID, amount),
                        MediaType.APPLICATION_JSON_TYPE));
        Assert.assertThat(response.getStatus(), Is.is(Response.Status.OK.getStatusCode()));

        final MoneyTransferInfo moneyTransferInfo = response.readEntity(MoneyTransferInfo.class);
        Assert.assertThat(moneyTransferInfo.getId(), CoreMatchers.notNullValue());
        Assert.assertThat(moneyTransferInfo.getBalance(), Is.is(balance));
    }
}
