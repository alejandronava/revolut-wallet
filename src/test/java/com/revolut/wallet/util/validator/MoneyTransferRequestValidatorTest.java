package com.revolut.wallet.util.validator;

import com.revolut.wallet.domain.vo.MoneyTransferRequest;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MoneyTransferRequestValidatorTest {

    private static MoneyTransferRequestValidator validator;

    @BeforeClass
    public static void setUp() {
        validator = new MoneyTransferRequestValidator();
    }

    @Test
    public void testIsValidAllInvalidArguments() {
        assertFalse(validator.isValid(new MoneyTransferRequest("", "", "", BigDecimal.ZERO)));
    }

    @Test
    public void testIsValidInvalidReceiverId() {
        assertFalse(validator.isValid(new MoneyTransferRequest(UUID.randomUUID().toString(), "", "", BigDecimal.ZERO)));
    }

    @Test
    public void testIsValidInvalidThirdPartyId() {
        assertFalse(validator.isValid(new MoneyTransferRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "", BigDecimal.ZERO)));
    }

    @Test
    public void testIsValidInvalidAmount() {
        assertFalse(validator.isValid(new MoneyTransferRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                UUID.randomUUID().toString(), BigDecimal.ZERO)));
    }

    @Test
    public void testIsValidInvalidSenderIdAndReceiverId() {
        final String id = UUID.randomUUID().toString();
        assertFalse(validator.isValid(new MoneyTransferRequest(id, id, UUID.randomUUID().toString(), BigDecimal.ZERO)));
    }

    @Test
    public void testIsValidInvalidRemoteAddress() {
        assertFalse(validator.isValid(new MoneyTransferRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                UUID.randomUUID().toString(), BigDecimal.ONE).withRemoteAddress("localhost")));
    }

    @Test
    public void testIsValid() {
        assertTrue(validator.isValid(new MoneyTransferRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                UUID.randomUUID().toString(), new BigDecimal(20.00)).withRemoteAddress("127.0.0.1")));
    }
}
