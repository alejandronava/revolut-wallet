package com.revolut.wallet.util;

import com.revolut.wallet.repository.DataSourceProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class ConnectionManagerThreadLocalImplIT {

    private static ConnectionManager txm;

    @BeforeClass
    public static void setUp() {
        txm = new ConnectionManagerThreadLocalImpl(DataSourceProvider.getInstance().getDataSource());
    }

    @Test
    public void testGet() {
        parallelRun(100, () -> assertNotNull(txm.get()));
    }

    @Test
    public void testBeginTransactionWhenIsAlreadyInitiated() {
        parallelRun(150, () -> {
            txm.beginTransaction();
            try {
                txm.beginTransaction();
            } catch (final IllegalStateException e) {
                txm.rollback();
                assertEquals("Transaction already initiated", e.getLocalizedMessage());
            }
        });
    }

    @Test
    public void testCommit() {
        parallelRun(100, () -> {
            txm.beginTransaction();
            assertNotNull(txm.get());
            txm.commit();
        });
    }

    @Test
    public void testRollback() {
        parallelRun(150, () -> {
            txm.beginTransaction();
            assertNotNull(txm.get());
            txm.rollback();
        });
    }

    private void parallelRun(int invocations, Runnable task) {
        IntStream.range(0, invocations).parallel().forEach(value -> task.run());
    }

}
