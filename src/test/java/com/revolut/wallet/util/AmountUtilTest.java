package com.revolut.wallet.util;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

@RunWith(DataProviderRunner.class)
public class AmountUtilTest {

    @DataProvider
    public static Object[][] amounts() {
        return new Object[][]{{14.5035, 14.50},{55.457, 55.46},{485.874, 485.87}};
    }

    @Test
    @UseDataProvider("amounts")
    public void testRound(final Double amount, final Double rounded) {
        Assert.assertThat(AmountUtil.round(new BigDecimal(amount)).doubleValue(), Is.is(rounded));
    }
}
