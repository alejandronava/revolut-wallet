package com.revolut.wallet.repository;

import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import javax.sql.DataSource;

public class DataSourceProviderIT {

    @Test
    public void testGetInstance() {
        DataSourceProvider provider = DataSourceProvider.getInstance();
        Assert.assertThat(DataSourceProvider.getInstance(), CoreMatchers.notNullValue());
        Assert.assertThat(DataSourceProvider.getInstance(), Is.is(provider));
    }

    @Test
    public void testGetDataSource() {
        DataSource dataSource = DataSourceProvider.getInstance().getDataSource();
        Assert.assertThat(dataSource, CoreMatchers.notNullValue());
        Assert.assertThat(DataSourceProvider.getInstance().getDataSource(), Is.is(dataSource));
    }
}
