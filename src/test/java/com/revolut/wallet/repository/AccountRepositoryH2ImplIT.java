package com.revolut.wallet.repository;

import com.revolut.wallet.TestDataSourceProvider;
import com.revolut.wallet.domain.Account;
import com.revolut.wallet.domain.vo.AccountOperation;
import com.revolut.wallet.util.AmountUtil;
import com.revolut.wallet.util.ConnectionManagerThreadLocalImpl;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

public class AccountRepositoryH2ImplIT {

    private static AccountRepository accountRepository;

    @BeforeClass
    public static void setUp() {
        accountRepository = new AccountRepositoryH2Impl(new ConnectionManagerThreadLocalImpl(
                TestDataSourceProvider.getInstance().getDataSource()));
    }

    @Test
    public void testFindByIdWhenExists() {
        Account account = accountRepository.getById("466049c8-2a34-45b3-941d-d2ce38903fe3");
        Assert.assertThat(account, CoreMatchers.notNullValue());
        Assert.assertThat(account.getId(), CoreMatchers.notNullValue());
        Assert.assertThat(account.getName(), Is.is("John Doe"));
        Assert.assertThat(account.getEmail(), Is.is("john.doe@test.io"));
        Assert.assertThat(account.getPhoneNumber(), Is.is("+14160000001"));
        Assert.assertThat(account.getBalance(), Is.is(AmountUtil.round(new BigDecimal(20.00))));
        Assert.assertThat(account.getCreatedAt(), CoreMatchers.notNullValue());
        Assert.assertThat(account.getUpdatedAt(), CoreMatchers.notNullValue());
    }

    @Test
    public void testFindByIdWhenNotExists() {
        Assert.assertThat(accountRepository.getById(UUID.randomUUID().toString()), CoreMatchers.nullValue());
    }

    @Test
    public void testUpdateBalance() {
        Account account = accountRepository.getById("cdfa6cf0-1f56-4040-897a-2a0787e58a9a");

        final BigDecimal previousBalance = account.getBalance();
        final AccountOperation.CreditOperation operation = new AccountOperation.CreditOperation(new BigDecimal(10.00));
        accountRepository.updateBalance(account, operation);

        account = accountRepository.getById(account.getId());
        Assert.assertThat(account.getBalance(), Is.is(previousBalance.add(operation.getAmount())));
    }
}
