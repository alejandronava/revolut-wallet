package com.revolut.wallet.repository;

import com.revolut.wallet.TestDataSourceProvider;
import com.revolut.wallet.domain.Transfer;
import com.revolut.wallet.util.ConnectionManagerThreadLocalImpl;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

public class TransferRepositoryH2ImplIT {

    private static TransferRepository repository;

    @BeforeClass
    public static void setUp() {
        repository = new TransferRepositoryH2Impl(new ConnectionManagerThreadLocalImpl(
                TestDataSourceProvider.getInstance().getDataSource()));
    }

    @Test
    public void testSave() {
        Transfer transfer = repository.save(new Transfer()
                .setSenderId("466049c8-2a34-45b3-941d-d2ce38903fe3")
                .setSenderPreviousBalance(new BigDecimal(20.00))
                .setSenderCurrentBalance(new BigDecimal(0.00))
                .setReceiverId("cdfa6cf0-1f56-4040-897a-2a0787e58a9a")
                .setReceiverPreviousBalance(new BigDecimal(0.00))
                .setReceiverCurrentBalance(new BigDecimal(20.00))
                .setThirdPartyId("945065b3-78d4-4717-a1d0-e41e08faa883")
                .setRemoteAddress("127.0.0.1")
                .setAmount(new BigDecimal(20.00)));
        Assert.assertThat(transfer, CoreMatchers.notNullValue());
        Assert.assertThat(transfer.getId(), CoreMatchers.notNullValue());
    }
}
