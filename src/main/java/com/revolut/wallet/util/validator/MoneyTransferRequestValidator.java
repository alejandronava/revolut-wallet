package com.revolut.wallet.util.validator;

import com.revolut.wallet.domain.vo.MoneyTransferRequest;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Pattern;

public final class MoneyTransferRequestValidator implements Validator<MoneyTransferRequest> {

    private static final Pattern IP_ADDRESS_PATTERN = Pattern.compile("([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})");

    @Override
    public boolean isValid(final MoneyTransferRequest object) {
        if (Objects.isNull(object.getSenderId()) || object.getSenderId().isEmpty()) {
            return false;
        }
        if (Objects.isNull(object.getReceiverId()) || object.getReceiverId().isEmpty()) {
            return false;
        }
        if (object.getSenderId().equals(object.getReceiverId())) {
            return false;
        }
        if (Objects.isNull(object.getThirdPartyId()) || object.getThirdPartyId().isEmpty()) {
            return false;
        }
        if (object.getAmount().doubleValue() <= BigDecimal.ZERO.doubleValue()) {
            return false;
        }
        return !(Objects.isNull(object.getRemoteAddress()) || object.getRemoteAddress().isEmpty()
                || !IP_ADDRESS_PATTERN.matcher(object.getRemoteAddress()).matches());
    }
}
