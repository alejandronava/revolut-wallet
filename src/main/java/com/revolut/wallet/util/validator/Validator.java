package com.revolut.wallet.util.validator;

public interface Validator<T> {

    boolean isValid(T object);
}
