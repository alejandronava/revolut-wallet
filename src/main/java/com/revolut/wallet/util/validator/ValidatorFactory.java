package com.revolut.wallet.util.validator;

import javax.inject.Inject;

public final class ValidatorFactory {

    private final MoneyTransferRequestValidator moneyTransferRequestValidator;

    @Inject
    public ValidatorFactory(final MoneyTransferRequestValidator moneyTransferRequestValidator) {
        this.moneyTransferRequestValidator = moneyTransferRequestValidator;
    }

    public MoneyTransferRequestValidator getMoneyTransferRequestValidator() {
        return moneyTransferRequestValidator;
    }
}
