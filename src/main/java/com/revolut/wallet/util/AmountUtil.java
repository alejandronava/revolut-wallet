package com.revolut.wallet.util;

import javax.xml.soap.SAAJMetaFactory;
import java.math.BigDecimal;

public final class AmountUtil {

    private static final int SCALE = 2;
    private static final int ROUNDING = BigDecimal.ROUND_HALF_DOWN;

    private AmountUtil() {
    }

    public static BigDecimal round(final BigDecimal amount) {
        return amount.setScale(SCALE, ROUNDING);
    }
}
