package com.revolut.wallet.util;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Stack;

public final class ConnectionManagerSingleImpl implements ConnectionManager {

    private final DataSource dataSource;
    private Connection connection;

    ConnectionManagerSingleImpl(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Connection get() {
        try {
            if (Objects.isNull(connection) || connection.isClosed()) {
                connection = dataSource.getConnection();
            }
            return connection;
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beginTransaction() {
        try {
            final Connection connection = get();
            if (!connection.getAutoCommit()) {
                throw new IllegalStateException("Transaction already initiated");
            }
            connection.setAutoCommit(false);
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void commit() {
        try {
            final Connection connection = get();
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            connection.setAutoCommit(true);
            connection.close();
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rollback() {
        try {
            final Connection connection = get();
            if (!connection.getAutoCommit()) {
                connection.rollback();
            }
            connection.setAutoCommit(true);
            connection.close();
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
