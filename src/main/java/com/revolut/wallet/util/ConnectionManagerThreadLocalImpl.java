package com.revolut.wallet.util;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;

public final class ConnectionManagerThreadLocalImpl implements ConnectionManager {

    private static ThreadLocal<ConnectionManager> manager;

    @Inject
    public ConnectionManagerThreadLocalImpl(final DataSource dataSource) {
        manager = ThreadLocal.withInitial(() -> new ConnectionManagerSingleImpl(dataSource));
    }

    @Override
    public Connection get() {
        return manager.get().get();
    }

    @Override
    public void beginTransaction() {
        manager.get().beginTransaction();
    }

    @Override
    public void commit() {
        manager.get().commit();
    }

    @Override
    public void rollback() {
        manager.get().rollback();
    }
}
