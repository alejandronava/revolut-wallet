package com.revolut.wallet.util;

import java.sql.Connection;

public interface ConnectionManager {

    Connection get();

    void beginTransaction();

    void commit();

    void rollback();
}
