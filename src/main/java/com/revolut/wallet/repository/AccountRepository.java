package com.revolut.wallet.repository;

import com.revolut.wallet.domain.Account;
import com.revolut.wallet.domain.vo.AccountOperation;

public interface AccountRepository {

    Account getById(String id);

    void updateBalance(Account account, AccountOperation operation);
}
