package com.revolut.wallet.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Optional;

public final class DataSourceProvider {

    private static DataSourceProvider instance;

    private final HikariDataSource dataSource;

    private DataSourceProvider() {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(Optional
                .ofNullable(System.getProperty("wallet.jdbc_url", System.getenv("WALLET_JDBC_URL")))
                .orElse("jdbc:h2:mem:wallet;DB_CLOSE_DELAY=-1"));
        config.setUsername(Optional.ofNullable(System.getenv("WALLET_JDBC_USER"))
                .orElse(""));
        config.setPassword(Optional.ofNullable(System.getenv("WALLET_JDBC_PASSWORD"))
                .orElse(""));
        dataSource = new HikariDataSource(config);

        migrate(dataSource);
    }

    private void migrate(final DataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.migrate();
    }

    public static synchronized DataSourceProvider getInstance() {
        if (Objects.isNull(instance)) {
            instance = new DataSourceProvider();
        }
        return instance;
    }

    public HikariDataSource getDataSource() {
        return dataSource;
    }
}
