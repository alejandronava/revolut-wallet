package com.revolut.wallet.repository;

import com.revolut.wallet.domain.Transfer;
import com.revolut.wallet.util.ConnectionManager;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class TransferRepositoryH2Impl implements TransferRepository {

    private static final String INSERT =
            "INSERT INTO transfers (id, sender_id, sender_pbalance, sender_cbalance, receiver_id, receiver_pbalance, " +
                    "               receiver_cbalance, third_party_id, remote_address, amount, created_at) " +
                    "       VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)";

    private final ConnectionManager connectionManager;

    @Inject
    public TransferRepositoryH2Impl(final ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Transfer save(final Transfer transfer) {
        final String id = UUID.randomUUID().toString();
        try (final Connection conn = connectionManager.get();
             final PreparedStatement pstm = conn.prepareStatement(INSERT)) {
            pstm.setString(1, id);
            pstm.setString(2, transfer.getSenderId());
            pstm.setBigDecimal(3, transfer.getSenderPreviousBalance());
            pstm.setBigDecimal(4, transfer.getSenderCurrentBalance());
            pstm.setString(5, transfer.getReceiverId());
            pstm.setBigDecimal(6, transfer.getReceiverPreviousBalance());
            pstm.setBigDecimal(7, transfer.getReceiverCurrentBalance());
            pstm.setString(8, transfer.getThirdPartyId());
            pstm.setString(9, transfer.getRemoteAddress());
            pstm.setBigDecimal(10, transfer.getAmount());
            int updated = pstm.executeUpdate();
            if (updated == 0) {
                return null;
            }
            return new Transfer(transfer).setId(id);
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
