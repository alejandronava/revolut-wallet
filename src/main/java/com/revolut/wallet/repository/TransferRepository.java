package com.revolut.wallet.repository;

import com.revolut.wallet.domain.Transfer;

public interface TransferRepository {

    Transfer save(Transfer transfer);
}
