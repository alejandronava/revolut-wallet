package com.revolut.wallet.repository;

import com.revolut.wallet.domain.Account;
import com.revolut.wallet.domain.vo.AccountOperation;
import com.revolut.wallet.util.ConnectionManager;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ConcurrentModificationException;

public class AccountRepositoryH2Impl implements AccountRepository {

    private static final String GET_BY_ID =
            "SELECT * FROM accounts WHERE id = ?";

    private static final String UPDATE_BALANCE =
            "UPDATE accounts " +
                    "       SET balance = balance + ?, " +
                    "           updated_at = CURRENT_TIMESTAMP " +
                    "     WHERE id = ? " +
                    "       AND balance = ?";

    private final ConnectionManager connectionManager;

    @Inject
    public AccountRepositoryH2Impl(final ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Account getById(final String id) {
        try (final Connection conn = connectionManager.get();
             final PreparedStatement pstm = conn.prepareStatement(GET_BY_ID)) {
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            if (!rs.next()) {
                return null;
            }
            return new Account()
                    .setId(rs.getString(1))
                    .setName(rs.getString(2))
                    .setEmail(rs.getString(3))
                    .setPhoneNumber(rs.getString(4))
                    .setBalance(rs.getBigDecimal(5))
                    .setCreatedAt(rs.getTimestamp(6).toInstant())
                    .setUpdatedAt(rs.getTimestamp(7).toInstant());
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateBalance(final Account account, final AccountOperation operation) {
        try (final Connection conn = connectionManager.get();
             final PreparedStatement pstm = conn.prepareStatement(UPDATE_BALANCE)) {
            pstm.setBigDecimal(1, operation.getAmount());
            pstm.setString(2, account.getId());
            pstm.setBigDecimal(3, account.getBalance());
            int updated = pstm.executeUpdate();
            if (updated == 0) {
                throw new ConcurrentModificationException();
            }
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
