package com.revolut.wallet;

import com.revolut.wallet.repository.*;
import com.revolut.wallet.resource.exception.InsufficientBalanceExceptionMapper;
import com.revolut.wallet.resource.exception.IllegalArgumentExceptionMapper;
import com.revolut.wallet.resource.exception.UncaughtExceptionMapper;
import com.revolut.wallet.service.MoneyTransferService;
import com.revolut.wallet.util.ConnectionManager;
import com.revolut.wallet.util.ConnectionManagerThreadLocalImpl;
import com.revolut.wallet.util.validator.MoneyTransferRequestValidator;
import com.revolut.wallet.util.validator.ValidatorFactory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;
import javax.sql.DataSource;
import javax.ws.rs.ext.ExceptionMapper;

public class ApplicationBinder extends AbstractBinder {

    private final DataSource dataSource;

    public ApplicationBinder(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void configure() {
        bind(dataSource).to(DataSource.class);
        bind(ConnectionManagerThreadLocalImpl.class).to(ConnectionManager.class).in(Singleton.class);

        bind(AccountRepositoryH2Impl.class).to(AccountRepository.class).in(Singleton.class);
        bind(TransferRepositoryH2Impl.class).to(TransferRepository.class).in(Singleton.class);

        bind(ValidatorFactory.class).to(ValidatorFactory.class).in(Singleton.class);
        bind(MoneyTransferRequestValidator.class).to(MoneyTransferRequestValidator.class).in(Singleton.class);

        bind(MoneyTransferService.class).to(MoneyTransferService.class).in(Singleton.class);

        bind(IllegalArgumentExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
        bind(InsufficientBalanceExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
        bind(UncaughtExceptionMapper.class).to(ExceptionMapper.class).in(Singleton.class);
    }
}
