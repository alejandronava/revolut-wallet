package com.revolut.wallet.service.exception;

public class InsufficientBalanceException extends java.lang.RuntimeException {

    public InsufficientBalanceException() {
        super("Insufficient balance to complete the transfer");
    }
}
