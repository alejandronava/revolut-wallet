package com.revolut.wallet.service;

import com.revolut.wallet.domain.Account;
import com.revolut.wallet.domain.Transfer;
import com.revolut.wallet.domain.vo.AccountOperation;
import com.revolut.wallet.domain.vo.MoneyTransferInfo;
import com.revolut.wallet.domain.vo.MoneyTransferRequest;
import com.revolut.wallet.repository.AccountRepository;
import com.revolut.wallet.repository.TransferRepository;
import com.revolut.wallet.service.exception.InsufficientBalanceException;
import com.revolut.wallet.util.ConnectionManager;
import com.revolut.wallet.util.validator.ValidatorFactory;

import javax.inject.Inject;

public class MoneyTransferService {

    private final AccountRepository accountRepository;
    private final ConnectionManager txm;
    private final TransferRepository transferRepository;
    private final ValidatorFactory validatorFactory;

    @Inject
    public MoneyTransferService(final AccountRepository accountRepository,
                                final ConnectionManager txm,
                                final TransferRepository transferRepository,
                                final ValidatorFactory validatorFactory) {
        this.accountRepository = accountRepository;
        this.txm = txm;
        this.transferRepository = transferRepository;
        this.validatorFactory = validatorFactory;
    }

    public MoneyTransferInfo send(final MoneyTransferRequest request) {
        if (!validatorFactory.getMoneyTransferRequestValidator().isValid(request)) {
            throw new IllegalArgumentException("Invalid request");
        }

        final Account senderAccount = accountRepository.getById(request.getSenderId());
        if (senderAccount.getBalance().doubleValue() < request.getAmount().doubleValue()) {
            throw new InsufficientBalanceException();
        }

        final Account receiverAccount = accountRepository.getById(request.getReceiverId());

        txm.beginTransaction();
        try {
            final Transfer transfer = transferRepository.save(new Transfer()
                    .setSenderId(senderAccount.getId())
                    .setSenderPreviousBalance(senderAccount.getBalance())
                    .setSenderCurrentBalance(senderAccount.getBalance().subtract(request.getAmount()))
                    .setReceiverId(receiverAccount.getId())
                    .setReceiverPreviousBalance(receiverAccount.getBalance())
                    .setReceiverCurrentBalance(receiverAccount.getBalance().add(request.getAmount()))
                    .setThirdPartyId(request.getThirdPartyId())
                    .setRemoteAddress(request.getRemoteAddress())
                    .setAmount(request.getAmount()));

            accountRepository
                    .updateBalance(senderAccount, new AccountOperation.DebitOperation(request.getAmount()));

            accountRepository
                    .updateBalance(receiverAccount, new AccountOperation.CreditOperation(request.getAmount()));

            txm.commit();

            return new MoneyTransferInfo(transfer.getId(), senderAccount.getBalance().subtract(request.getAmount()));
        } catch (final Exception e) {
            txm.rollback();
            throw e;
        }
    }
}
