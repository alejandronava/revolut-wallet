package com.revolut.wallet.resource.exception;

import com.revolut.wallet.domain.vo.ErrorResponse;
import com.revolut.wallet.service.exception.InsufficientBalanceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InsufficientBalanceExceptionMapper implements ExceptionMapper<InsufficientBalanceException> {

    @Override
    public Response toResponse(final InsufficientBalanceException exception) {
        return Response
                .status(Response.Status.PRECONDITION_FAILED)
                .entity(new ErrorResponse(exception.getLocalizedMessage()))
                .build();
    }
}
