package com.revolut.wallet.resource.exception;

import com.revolut.wallet.domain.vo.ErrorResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

    @Override
    public Response toResponse(IllegalArgumentException exception) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(exception.getLocalizedMessage()))
                .build();
    }
}
