package com.revolut.wallet.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class UncaughtExceptionMapper implements ExceptionMapper<RuntimeException> {

    @Override
    public Response toResponse(final RuntimeException exception) {
        return Response.status(500).entity("Something bad happened. Please try again !!").type("text/plain").build();
    }
}
