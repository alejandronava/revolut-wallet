package com.revolut.wallet.resource;

import com.revolut.wallet.domain.vo.MoneyTransferRequest;
import com.revolut.wallet.service.MoneyTransferService;
import com.revolut.wallet.service.exception.InsufficientBalanceException;
import org.glassfish.grizzly.http.server.Request;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("money-transfer")
public final class MoneyTransferResource {

    private final MoneyTransferService moneyTransferService;

    @Inject
    public MoneyTransferResource(final MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response send(@Context Request request, final MoneyTransferRequest moneyTransferRequest)
            throws InsufficientBalanceException {
        moneyTransferRequest.withRemoteAddress(request.getRemoteAddr());
        return Response.ok(moneyTransferService.send(moneyTransferRequest)).build();
    }
}
