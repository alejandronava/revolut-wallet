package com.revolut.wallet.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

    private final String message;

    public ErrorResponse(@JsonProperty("message") final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
