package com.revolut.wallet.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.wallet.util.AmountUtil;

import java.math.BigDecimal;
import java.util.Objects;

public final class MoneyTransferInfo {

    private String id;
    private BigDecimal balance;

    public MoneyTransferInfo(@JsonProperty("id") final String id, @JsonProperty("balance") final BigDecimal balance) {
        this.id = id;
        this.balance = AmountUtil.round(balance);
    }

    public String getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MoneyTransferInfo that = (MoneyTransferInfo) o;
        return id.equals(that.id) &&
                balance.equals(that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }

    @Override
    public String toString() {
        return "TransferInfo{" +
                "id='" + id + '\'' +
                ", balance=" + balance +
                '}';
    }
}
