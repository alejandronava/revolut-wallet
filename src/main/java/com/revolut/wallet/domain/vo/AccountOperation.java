package com.revolut.wallet.domain.vo;

import com.revolut.wallet.util.AmountUtil;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountOperation {

    private final BigDecimal amount;

    AccountOperation(final BigDecimal amount, final boolean credit) {
        if (amount.doubleValue() <= BigDecimal.ZERO.doubleValue()) {
            throw new IllegalArgumentException("Amount should be greater than zero");
        }
        this.amount = AmountUtil.round(credit ? amount : amount.negate());
    }

    public final BigDecimal getAmount() {
        return amount;
    }

    public static final class DebitOperation extends AccountOperation {

        public DebitOperation(final BigDecimal amount) {
            super(amount, false);
        }

        @Override
        public String toString() {
            return "DebitOperation{" +
                    "amount=" + getAmount() +
                    '}';
        }
    }

    public static final class CreditOperation extends AccountOperation {

        public CreditOperation(final BigDecimal amount) {
            super(amount, true);
        }

        @Override
        public String toString() {
            return "CreditOperation{" +
                    "amount=" + getAmount() +
                    '}';
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AccountOperation that = (AccountOperation) o;
        return amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
