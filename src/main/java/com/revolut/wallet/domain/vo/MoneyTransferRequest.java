package com.revolut.wallet.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.wallet.util.AmountUtil;

import java.math.BigDecimal;

public final class MoneyTransferRequest {

    private final String senderId;
    private final String receiverId;
    private final String thirdPartyId;
    private final BigDecimal amount;
    private String remoteAddress;

    public MoneyTransferRequest(@JsonProperty("senderId") final String senderId,
                                @JsonProperty("receiverId") final String receiverId,
                                @JsonProperty("thirdPartyId") final String thirdPartyId,
                                @JsonProperty("amount") final BigDecimal amount) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.thirdPartyId = thirdPartyId;
        this.amount = AmountUtil.round(amount);
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public MoneyTransferRequest withRemoteAddress(final String remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    @JsonIgnore
    public String getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public String toString() {
        return "MoneyTransferRequest{" +
                "senderId='" + senderId + '\'' +
                ", receiverId='" + receiverId + '\'' +
                ", thirdPartyId='" + thirdPartyId + '\'' +
                ", amount=" + amount +
                ", remoteAddress='" + remoteAddress + '\'' +
                '}';
    }
}
