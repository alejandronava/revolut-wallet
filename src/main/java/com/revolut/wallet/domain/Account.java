package com.revolut.wallet.domain;

import com.revolut.wallet.util.AmountUtil;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public final class Account {

    private String id;
    private String name;
    private String email;
    private String phoneNumber;
    private BigDecimal balance;
    private Instant createdAt;
    private Instant updatedAt;

    public String getId() {
        return id;
    }

    public Account setId(final String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Account setName(final String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Account setEmail(final String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Account setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Account setBalance(final BigDecimal balance) {
        this.balance = AmountUtil.round(balance);
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Account setCreatedAt(final Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Account setUpdatedAt(final Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Account account = (Account) o;
        return id.equals(account.id) &&
                name.equals(account.name) &&
                email.equals(account.email) &&
                phoneNumber.equals(account.phoneNumber) &&
                balance.equals(account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, phoneNumber, balance);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", balance=" + balance +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
