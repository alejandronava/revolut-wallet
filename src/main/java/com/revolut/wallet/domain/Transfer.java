package com.revolut.wallet.domain;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public final class Transfer {

    private String id;
    private String senderId;
    private BigDecimal senderPreviousBalance;
    private BigDecimal senderCurrentBalance;
    private String receiverId;
    private BigDecimal receiverPreviousBalance;
    private BigDecimal receiverCurrentBalance;
    private String thirdPartyId;
    private String remoteAddress;
    private BigDecimal amount;
    private Instant createdAt;

    public Transfer() {
    }

    public Transfer(final Transfer other) {
        this.id = other.id;
        this.senderId = other.senderId;
        this.senderPreviousBalance = other.senderPreviousBalance;
        this.senderCurrentBalance = other.senderCurrentBalance;
        this.receiverId = other.receiverId;
        this.receiverPreviousBalance = other.receiverPreviousBalance;
        this.receiverCurrentBalance = other.receiverCurrentBalance;
        this.thirdPartyId = other.thirdPartyId;
        this.remoteAddress = other.remoteAddress;
        this.amount = other.amount;
        this.createdAt = other.createdAt;
    }

    public String getId() {
        return id;
    }

    public Transfer setId(final String id) {
        this.id = id;
        return this;
    }

    public String getSenderId() {
        return senderId;
    }

    public Transfer setSenderId(final String senderId) {
        this.senderId = senderId;
        return this;
    }

    public BigDecimal getSenderPreviousBalance() {
        return senderPreviousBalance;
    }

    public Transfer setSenderPreviousBalance(final BigDecimal senderPreviousBalance) {
        this.senderPreviousBalance = senderPreviousBalance;
        return this;
    }

    public BigDecimal getSenderCurrentBalance() {
        return senderCurrentBalance;
    }

    public Transfer setSenderCurrentBalance(final BigDecimal senderCurrentBalance) {
        this.senderCurrentBalance = senderCurrentBalance;
        return this;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public Transfer setReceiverId(final String receiverId) {
        this.receiverId = receiverId;
        return this;
    }

    public BigDecimal getReceiverPreviousBalance() {
        return receiverPreviousBalance;
    }

    public Transfer setReceiverPreviousBalance(final BigDecimal receiverPreviousBalance) {
        this.receiverPreviousBalance = receiverPreviousBalance;
        return this;
    }

    public BigDecimal getReceiverCurrentBalance() {
        return receiverCurrentBalance;
    }

    public Transfer setReceiverCurrentBalance(final BigDecimal receiverCurrentBalance) {
        this.receiverCurrentBalance = receiverCurrentBalance;
        return this;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public Transfer setThirdPartyId(final String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
        return this;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public Transfer setRemoteAddress(final String remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Transfer setAmount(final BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Transfer setCreatedAt(final Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Transfer transfer = (Transfer) o;
        return Objects.equals(id, transfer.id) &&
                senderId.equals(transfer.senderId) &&
                senderPreviousBalance.equals(transfer.senderPreviousBalance) &&
                senderCurrentBalance.equals(transfer.senderCurrentBalance) &&
                receiverId.equals(transfer.receiverId) &&
                receiverPreviousBalance.equals(transfer.receiverPreviousBalance) &&
                receiverCurrentBalance.equals(transfer.receiverCurrentBalance) &&
                thirdPartyId.equals(transfer.thirdPartyId) &&
                remoteAddress.equals(transfer.remoteAddress) &&
                amount.equals(transfer.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, senderId, senderPreviousBalance, senderCurrentBalance, receiverId, receiverPreviousBalance, receiverCurrentBalance, thirdPartyId, remoteAddress, amount);
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id='" + id + '\'' +
                ", senderId='" + senderId + '\'' +
                ", senderPreviousBalance=" + senderPreviousBalance +
                ", senderCurrentBalance=" + senderCurrentBalance +
                ", receiverId='" + receiverId + '\'' +
                ", receiverPreviousBalance=" + receiverPreviousBalance +
                ", receiverCurrentBalance=" + receiverCurrentBalance +
                ", thirdPartyId='" + thirdPartyId + '\'' +
                ", remoteAddress='" + remoteAddress + '\'' +
                ", amount=" + amount +
                ", createdAt=" + createdAt +
                '}';
    }
}
