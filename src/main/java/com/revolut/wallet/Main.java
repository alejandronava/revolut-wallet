package com.revolut.wallet;

import com.revolut.wallet.repository.DataSourceProvider;
import com.revolut.wallet.resource.MoneyTransferResource;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class Main {

    private static final String BASE_URI = "http://localhost:8080/wallet/";

    @SuppressWarnings("all")
    public static void main(final String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }

    private static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig();
        rc.register(new ApplicationBinder(DataSourceProvider.getInstance().getDataSource()));
        rc.register(MoneyTransferResource.class);
        rc.register(JacksonFeature.class);
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }
}
