INSERT INTO accounts (id, name, email, phone_number, balance, created_at, updated_at)
     VALUES ('466049c8-2a34-45b3-941d-d2ce38903fe3', 'John Doe', 'john.doe@test.io', '+14160000001', 20.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO accounts (id, name, email, phone_number, balance, created_at, updated_at)
     VALUES ('cdfa6cf0-1f56-4040-897a-2a0787e58a9a', 'Joe Doe', 'joe.doe@test.io', '+14160000002', 0.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);