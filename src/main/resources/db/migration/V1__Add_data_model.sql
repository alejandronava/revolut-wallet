CREATE TABLE accounts (
    id CHAR(36),
    name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    balance DECIMAL(20, 2) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (email)
);

CREATE TABLE third_parties (
    id CHAR(36),
    name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (email)
);

CREATE TABLE transfers (
    id CHAR(36),
    sender_id CHAR(36)  NOT NULL REFERENCES accounts(id),
    sender_pbalance DECIMAL(20, 2) NOT NULL,
    sender_cbalance DECIMAL(20, 2) NOT NULL,
    receiver_id CHAR(36) NOT NULL REFERENCES accounts(id),
    receiver_pbalance DECIMAL(20, 2) NOT NULL,
    receiver_cbalance DECIMAL(20, 2) NOT NULL,
    third_party_id CHAR(36) NOT NULL REFERENCES third_parties(id),
    remote_address CHAR(15) NOT NULL,
    amount DECIMAL(20, 2) NOT NULL,
    created_at TIMESTAMP NOT NULL
);